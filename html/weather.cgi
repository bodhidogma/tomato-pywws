#! /bin/sh

echo "Content-type: text/html"
echo

# Design ......... :  Nature vegetal (C)2012 http://www.lady-beetle.com
# Weather template : Weather by You! (C)2012 http://www.weatherbyyou.com
# Date ........... : 2012/06/04

cat <<-HERE
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html;" />
  <title>pywws - KCAREDWO25 Weather</title>
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="expires" content="-1" />
  <meta http-equiv="Cache-control" content="no-cache" />
  <link href="styles.css" rel="stylesheet" type="text/css" media="all" />
  <link rel="shortcut icon" href="favicon.ico" />
  <script type="text/javascript"> <!--
    // Month and day names. Replace by your own for foreign languages
    var DayName = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    var MonthName = new Array("January", "Febuary", "March", "April", "May", "June", "July", 
        "August", "September", "October", "November", "December");
    //-->
  </script>
</head>
<body>
<div id="page">
  <div id="header">
    <!-- Text to be adapted for your site name and slogan  -->
    <div id="header_part1">pywws<br /><span>Python software for USB Wireless WeatherStations</span></div>
    <div id="header_part2"></div>
  </div>
  <!--  Section for main menu, all grey bar long  -->
  <div id="menubar">
    <div id="header_left">
      <!--  Script to insert today's date  -->
      <script type="text/javascript"> <!--
        var CurrentDate = new Date();
        var DateCur = CurrentDate.getDate();
        var DayNumber = CurrentDate.getDay();
        var MonthNumber = CurrentDate.getMonth();
        var Year = CurrentDate.getFullYear();
        document.write(DayName[DayNumber] + ", " + DateCur + " " + MonthName[MonthNumber] + " " + Year); //-->
      </script>
    </div>
    <a id="home" href="weather.cgi"> >> Home</a>
    <div id="mainmenu">
      <ul>
        <li><a href="?1hrs" >1 hour</a></li>
        <li><a href="?6hrs" >6 hour</a></li>
        <li><a href="?24hrs" >24 hours</a></li>
        <li><a href="?7days" >7 days</a></li>
        <li><a href="?allmonths" >Months</a></li>
      </ul>
    </div>
    <div id="cemter_menubar">Observations for last</div>
  </div>
  <div id="content_head"></div>
  <div id="content">
    <!--  Left side panel  -->
    <div id="left_col">
      <h2 class="no_margin">Forecast</h2>
      <div id="today_forecast" class="forecast"></div>
HERE

cat data/forecast_icon.txt

cat <<-HERE
      <br /><hr />
      <h3 class="no_margin">Graphs</h3>
      <hr />
      <!-- Left side menu  -->
      <ul class="sidemenu">
        <li><a href="?24hrs.png">24 hours</a></li>
        <li><a href="?24hrs_full_features.png">24 hours full</a></li>
        <li><a href="?7days.png">7 days</a></li>
        <li><a href="?28days.png">28 days</a></li>
        <li><a href="?rose_1hr.png">Wind: 1 hour</a></li>
        <li><a href="?rose_12hrs.png">Wind: 12 hours</a></li>
        <li><a href="?rose_24hrs.png">Wind: 24 hours</a></li>
        <li><a href="?12mos.png">12 Months</a></li>
      </ul>
      <br /><hr />
      <h3 class="no_margin">Links</h3>
      <hr />
      <ul class="sidemenu">
        <li><a href="http://weather.weatherbug.com/">WeatherBug</a></li>
        <li><a href="http://www.wunderground.com/">Weather Underground</a></li>
      </ul>
    </div>
    <!--  Right side (main) panel  -->
    <div id="main_panel">
      <!-- Container for swapping content  -->
      <div id="weather_infos">
HERE

QS=$QUERY_STRING
case $QS in
	1hrs ) cat data/1hrs.txt ;; 
	6hrs ) cat data/6hrs.txt ;; 
	24hrs ) cat data/24hrs.txt ;;
	7days ) cat data/7days.txt ;;
	allmonths ) cat data/allmonths.txt ;;
	24hrs.png ) 
		echo '<h2>Last 24 hours graphs</h2>'
		echo '<p><img src="data/24hrs.png" alt="" title=""  /></p>'
		;;
	24hrs_full_features.png )
		echo '<h2>Last 24 hours graphs</h2>';
		echo '<p><img src="data/24hrs_full_features.png" alt="" title=""  /></p>';
		;;
	7days.png ) 
		echo '<h2>Last 7 days graphs</h2>'
                echo '<p><img src="data/7days.png" alt="" title=""  /></p>';
		;;
	28days.png ) 
		echo '<h2>Last 28 days graphs</h2>'
                echo '<p><img src="data/28days.png" alt="" title=""  /></p>';
		;;
	rose_1hr.png ) 
                echo '<p><img src="data/rose_1hr.png" alt="" title=""  /></p>';
		;;
	rose_12hrs.png ) 
                echo '<p><img src="data/rose_12hrs.png" alt="" title=""  /></p>';
		;;
	rose_24hrs.png ) 
                echo '<p><img src="data/rose_24hrs.png" alt="" title=""  /></p>';
		;;
	12mos.png ) 
		echo '<h2>Last 12 months graphs</h2>'
                echo '<p><img src="data/12mos.png" alt="" title=""  /></p>';
		;;
	*) cat index.txt data/live.txt ;;
esac

cat <<-HERE
      </div>
    </div>
    <div style="clear: both"></div>
  </div>
  <div id="content_footer"></div>
  <div id="footer">
    <a id="pywws-link" href="http://code.google.com/p/pywws/">&nbsp;</a>
    <p>&copy;<small> 2013</small> KCAREDWO25 Weather</p>
  </div>
  <div id="copyright">
    Provided By <a href="http://www.weatherbyyou.com/" title="Free Weather website templates">Weather by You � 2012</a> <small>|</small> <a href="http://www.lady-beetle.com/" title="Free Templates">� 2009 Lady Beetle</a>
  </div>
</div>
</body>
</html>
HERE
