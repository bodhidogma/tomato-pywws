[paths]
templates = /opt/home/weather/templates/
graph_templates = /opt/home/weather/graph_templates/
work = /opt/home/weather/tmp

[Zambretti]
baro upper = 1050.0
north = True
baro lower = 950.0

[hourly]
last update = 2013-04-09 20:04:00
plot = ['rose_12hrs.png.xml','rose_24hrs.png.xml']
text = ['feed_hourly.xml','6hrs.txt','j/6hrs_json.txt','j/6hrs_wind.txt','24hrs.txt','forecast_icon.txt']
twitter = []
services = ['underground']

[logged]
last update = 2013-04-09 20:09:00
plot = ['rose_1hr.png.xml','24hrs_full_features.png.xml','24hrs.png.xml']
text = ['1hrs.txt']
twitter = []
services = []

[daily]
last update = 2013-04-09 18:29:00
plot = ['7days.png.xml','12mos.png.xml','28days.png.xml','rose_7days_nights.png.xml']
text = ['feed_daily.xml','forecast_week.txt','7days.txt','allmonths.txt']
twitter = []
services = []

[live]
services = ['underground']
twitter = []
plot = []
text = ['live.txt','j/live_json.txt']

[underground]
last update = 2013-04-09 20:08:23
password = secret
station = KPYWWS

[ftp]
secure = False
site = ftp.username.your_isp.co.uk
local site = True
user = username
directory = /opt/home/weather/html/data
password = secret

[fixed]
fixed block = {'data_changed': 0, 'unknown_18': 0, 'timezone': -8, 'data_count': 4080, 'min': {'hum_out': {'date': '2013-02-26 15:12', 'val': 15}, 'windchill': {'date': '2013-01-13 07:19', 'val': -2.5}, 'dewpoint': {'date': '2013-01-13 12:47', 'val': -12.200000000000001}, 'temp_in': {'date': '2013-03-04 17:02', 'val': 15.0}, 'abs_pressure': {'date': '2013-02-19 17:12', 'val': 989.7}, 'rel_pressure': {'date': '2013-04-08 03:01', 'val': 1005.9}, 'hum_in': {'date': '2013-03-23 14:05', 'val': 23}, 'temp_out': {'date': '2013-01-13 07:19', 'val': -2.5}}, 'lux_wm2_coeff': 0, 'abs_pressure': 998.8, 'alarm_1': {'hum_out_hi': False, 'hum_in_lo': False, 'hum_in_hi': False, 'hum_out_lo': False, 'time': False, 'bit3': False, 'wind_dir': False, 'bit0': False}, 'alarm_3': {'temp_out_hi': False, 'wind_chill_lo': False, 'dew_point_lo': False, 'temp_in_lo': False, 'wind_chill_hi': False, 'temp_in_hi': False, 'temp_out_lo': False, 'dew_point_hi': False}, 'alarm_2': {'wind_ave': False, 'wind_gust': False, 'rain_hour': False, 'pressure_rel_lo': False, 'pressure_abs_hi': False, 'rain_day': False, 'pressure_rel_hi': False, 'pressure_abs_lo': False}, 'max': {'hum_out': {'date': '2013-01-09 00:16', 'val': 99}, 'windchill': {'date': '2010-01-01 12:00', 'val': 27.6}, 'dewpoint': {'date': '2010-01-01 12:00', 'val': 17.5}, 'uv': {'val': 0}, 'wind_ave': {'date': '2013-03-08 15:49', 'val': 4.4000000000000006}, 'rain': {'week': {'date': '2013-03-08 01:05', 'val': 38.699999999999994}, 'total': {'date': '2013-03-08 01:05', 'val': 101.7}, 'day': {'date': '2013-02-19 17:02', 'val': 22.5}, 'hour': {'date': '2010-01-01 12:04', 'val': 19.8}, 'month': {'date': '2013-03-08 01:05', 'val': 72.0}}, 'temp_in': {'date': '2010-01-01 12:00', 'val': 29.7}, 'illuminance': {'val': 0}, 'abs_pressure': {'date': '2010-01-01 12:00', 'val': 1013.8}, 'rel_pressure': {'date': '2013-01-15 09:42', 'val': 1030.5}, 'hum_in': {'date': '2013-01-08 16:43', 'val': 75}, 'temp_out': {'date': '2010-01-01 12:00', 'val': 27.6}, 'wind_gust': {'date': '2013-04-08 03:13', 'val': 10.9}}, 'settings_1': {'pressure_inHg': True, 'pressure_hPa': False, 'temp_out_F': True, 'pressure_mmHg': False, 'rain_in': True, 'temp_in_F': True, 'bit4': False, 'bit3': False}, 'settings_2': {'wind_bft': False, 'wind_mps': False, 'wind_knot': False, 'bit7': False, 'bit6': False, 'bit5': False, 'wind_kmph': False, 'wind_mph': True}, 'read_period': 5, 'rel_pressure': 1013.3, 'unknown_01': 0, 'date_time': '2013-04-09 13:04', 'current_pos': 7856, 'display_2': {'temp_out_temp': True, 'rain_hour': False, 'rain_month': False, 'rain_week': False, 'temp_out_chill': False, 'rain_day': True, 'temp_out_dew': False, 'rain_total': False}, 'display_3': {'illuminance_fc': False, 'bit7': False, 'bit6': False, 'bit5': False, 'bit4': False, 'bit3': False, 'bit2': False, 'bit1': False}, 'alarm': {'hum_out': {'lo': 45, 'hi': 70}, 'windchill': {'lo': 32.0, 'hi': 68.0}, 'dewpoint': {'lo': 14.0, 'hi': 50.0}, 'temp_out': {'lo': 14.0, 'hi': 86.0}, 'uv': 0, 'wind_ave': {'ms': 12.100000000000001, 'bft': 3}, 'rain': {'day': 59.1, 'hour': 1.2}, 'temp_in': {'lo': 32.0, 'hi': 68.0}, 'illuminance': 0, 'abs_pressure': {'lo': 283.5, 'hi': 307.20000000000006}, 'rel_pressure': {'lo': 283.5, 'hi': 307.20000000000006}, 'hum_in': {'lo': 35, 'hi': 66}, 'time': '07:00', 'wind_dir': 0, 'wind_gust': {'ms': 23.900000000000003, 'bft': 5}}, 'display_1': {'wind_gust': False, 'show_day_name': True, 'show_year': False, 'time_scale_24': False, 'pressure_rel': True, 'alarm_time': False, 'date_mdy': False, 'clock_12hr': False}}
ws type = 1080
pressure offset = 14.5

[config]
logdata sync = 1
gnuplot encoding = iso_8859_1
day end hour = 23

[12 hourly]
last update = 2013-04-09 19:09:00
plot = []
text = []
twitter = []
services = []

