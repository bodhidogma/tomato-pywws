tomato-pywws
============

Support scripts / documentation for pywws installation on a tomato (or other) router device.

The web HTML templates are modifications of those found on http://www.weatherbyyou.com.
The plotting and text templates are modified from those found on the above site as well as
reading through the sources and playing around with it till I was able to get everything 
the way I wanted it.

I am currently running this on an Asus N16 router with Tomato USB installed.
The webserver is thttpd enabled to support CGI which simplifies the HTML page delivery.
I suppose it might be easier to FTP all the data to a production server using PHP, but
where is the fun in that? ;)

NOTE:
This project is deprecated.  Currently using http://www.weewx.com/ as is highly reliable and customizable.
